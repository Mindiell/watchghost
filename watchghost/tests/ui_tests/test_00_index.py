import os

HOST = os.getenv('WATCHGHOST_HOST_TEST', '0.0.0.0')
URL = f"http://{HOST}:8888"


def assert_watcher_details(selenium):
    navbar = selenium.find_element_by_class_name('navbar-brand').text
    assert "WatchGhost" in navbar

    watcher_details = selenium.find_element_by_tag_name('dl')
    watcher_dt = watcher_details.find_elements_by_tag_name('dt')
    watcher_dd = watcher_details.find_elements_by_tag_name('dd')

    assert watcher_dt[0].text == 'Service'
    assert watcher_dd[0].text == 'Ping IPv4'
    assert watcher_dt[1].text == 'Status'
    assert watcher_dd[1].text == 'info'
    assert watcher_dt[2].text == 'Next check'
    assert '(check now)' in watcher_dd[2].text

    assert watcher_dt[3].text == 'Last result'
    last_result_details = watcher_dd[3].find_elements_by_tag_name('dt')
    assert last_result_details[0].text == 'Is hard'
    assert last_result_details[1].text == 'Start'
    assert last_result_details[2].text == 'End'
    assert last_result_details[3].text == 'Duration'
    assert last_result_details[4].text == 'Response'


def test_dashboard(selenium):
    selenium.get(URL)
    navbar = selenium.find_element_by_class_name('navbar-brand').text
    assert "WatchGhost" in navbar

    hide_info = selenium.find_elements_by_id('checkbox_hide_info')
    hide_info[0].click()

    cards = selenium.find_elements_by_class_name('card')

    # localhost server
    card_header = cards[0].find_elements_by_class_name('card-header')
    assert "localhost" in card_header[0].text

    watchers = cards[0].find_elements_by_class_name('list-group-item')
    assert "Ping IPv4" in watchers[0].text
    assert "Ping IPv6" in watchers[1].text
    assert "http://localhost:8888/" in watchers[2].text
    assert "Expire - localg.host" in watchers[3].text


def test_click_on_watcher(selenium):
    selenium.get(URL)
    selenium.find_elements_by_id('checkbox_hide_info')[0].click()

    cards = selenium.find_elements_by_class_name('card')
    watchers = cards[0].find_elements_by_class_name('list-group-item')
    watchers[0].find_element_by_tag_name('a').click()

    assert_watcher_details(selenium)


def test_watcher_url(selenium):
    selenium.get("{}/watchers/localhost_ping4".format(URL))
    assert_watcher_details(selenium)
